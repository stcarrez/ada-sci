-- --------------------------------------------------------------------
--  sci-testsuite -- SCI testsuite
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package SCI.Testsuite is

   function Suite return Util.Tests.Access_Test_Suite;

end SCI.Testsuite;
