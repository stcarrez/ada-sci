-- --------------------------------------------------------------------
--  sci-testsuite -- SCI testsuite
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with SCI.Similarities.Tests;
with SCI.Stat_Tests;
with SCI.Corr_Tests;
package body SCI.Testsuite is

   Tests : aliased Util.Tests.Test_Suite;

   function Suite return Util.Tests.Access_Test_Suite is
   begin
      SCI.Similarities.Tests.Add_Tests (Tests'Access);
      SCI.Stat_Tests.Add_Tests (Tests'Access);
      SCI.Corr_Tests.Add_Tests (Tests'Access);
      return Tests'Access;
   end Suite;

end SCI.Testsuite;
