-- --------------------------------------------------------------------
--  sci-cor_tests -- Tests for correlations generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package SCI.Corr_Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Util.Tests.Test with null record;

   --  Tests pearson correltation.
   procedure Test_Pearson_Correlation (T : in out Test);

end SCI.Corr_Tests;
