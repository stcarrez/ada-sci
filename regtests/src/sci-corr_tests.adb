-- --------------------------------------------------------------------
--  sci-cor_tests -- Tests for correlations generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Util.Test_Caller;
with SCI.Correlations.Pearson;
package body SCI.Corr_Tests is

   package Caller is new Util.Test_Caller (Test, "SCI.Correlations");

   type Degree_Type is digits 10 range -273.0 .. 1_000.0;
   type Degree_Array is array (Positive range <>) of Degree_Type;

   --  Use Float for internal computation.
   package Float_Correlations is
     new SCI.Correlations (Float);

   --  Use Degree_Type for values.
   package Degree_Correlations is
     new Float_Correlations.Pearson (Value_Type => Degree_Type);

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Correlations.Pearson",
                       Test_Pearson_Correlation'Access);
   end Add_Tests;

   --  Tests descriptive stats on bounded float values.
   procedure Test_Pearson_Correlation (T : in out Test) is
      function Correlation is
         new Degree_Correlations.Correlation (Index_Type => Positive,
                                              Array_Type => Degree_Array);
      V1  : constant Degree_Array := (-10.0, 1.0, 2.0, 3.0, 100.0);
      V2  : Degree_Array := (-10.0 * 2.0, 1.0 * 1.5, 2.0, 3.0 * 0.5, 100.0 * 0.25);
      Rho : Float;
   begin
      Rho := Correlation (V1, V2);
      Ada.Text_IO.Put_Line ("Rho=" & Rho'Image);
      T.Assert (Rho >= 0.869 and then Rho <= 0.87, "Invalid correlation");

      for I in V1'Range loop
         V2 (I) := V1 (I) * 2.0;
      end loop;
      Rho := Correlation (V1, V2);
      Ada.Text_IO.Put_Line ("Rho=" & Rho'Image);
      T.Assert (Rho = 1.0, "Invalid correlation");

      V2 := (10.0, 100.0, 20.0, 30.0, 1.0);
      Rho := Correlation (V1, V2);
      Ada.Text_IO.Put_Line ("Rho=" & Rho'Image);
      T.Assert (Rho >= -0.4 and then Rho <= -0.399, "Invalid correlation");

   end Test_Pearson_Correlation;

end SCI.Corr_Tests;
