-- --------------------------------------------------------------------
--  sci-similarities-tests -- Tests for similarities generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Util.Test_Caller;
with SCI.Statistics.Descriptive;
package body SCI.Stat_Tests is

   package Caller is new Util.Test_Caller (Test, "SCI.Statistics");

   package Float_Statistics is new SCI.Statistics (Float);

   type Degree_Type is digits 10 range -273.0 .. 1_000.0;
   type Degree_Array is array (Positive range <>) of Degree_Type;

   package Degree_Statistics is
     new Float_Statistics.Descriptive (Value_Type => Degree_Type,
                                       Index_Type => Positive,
                                       Array_Type => Degree_Array);

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Statistics.Descriptive",
                       Test_Stats_Bounded'Access);
   end Add_Tests;

   --  Tests descriptive stats on bounded float values.
   procedure Test_Stats_Bounded (T : in out Test) is
      V : Degree_Array := (-10.0, 1.0, 2.0, 3.0, 100.0);
      R : Degree_Statistics.Stats_Type;
      D : Float;
   begin
      R := Degree_Statistics.Compute (V);
      Ada.Text_IO.Put_Line ("Sq=" & R.Sum_Square'Image);
      T.Assert (R.Min = V (1), "Invalid min value");
      T.Assert (R.Max = V (5), "Invalid max value");
      T.Assert (R.Sum >= 95.0, "Invalid sum");
      T.Assert (R.Mean >= 19.2 and then R.Mean <= 19.3, "Invalid mean");
      T.Assert (R.Sum_Square >= 10114.0 and then R.Sum_Square <= 10115.0,
                "Invalid square");
      D := Degree_Statistics.Deviation (V, R);
      Ada.Text_IO.Put_Line ("D=" & D'Image);
      T.Assert (D >= 40.6 and then D <= 40.7,
                "Invalid deviation");
      T.Assert (D = Degree_Statistics.Deviation (V), "Invalid deviation");

      V (2) := Degree_Type'First;
      V (3) := Degree_Type'Last;
      R := Degree_Statistics.Compute (V);
      Ada.Text_IO.Put_Line ("Sq=" & R.Sum_Square'Image);
      T.Assert (R.Min = Degree_Type'First, "Invalid min value");
      T.Assert (R.Max = Degree_Type'Last, "Invalid max value");
      T.Assert (R.Sum = 820.0, "Invalid sum");
      T.Assert (R.Mean >= 164.0 and then R.Mean <= 164.1, "Invalid mean");
      T.Assert (R.Sum_Square >= 1_084_600.0 and then R.Sum_Square <= 1_084_650.0,
                "Invalid square");
      D := Degree_Statistics.Deviation (V, R);
      Ada.Text_IO.Put_Line ("D=" & D'Image);
      T.Assert (D >= 435.9 and then D <= 436.0,
                "Invalid deviation");
      T.Assert (D = Degree_Statistics.Deviation (V), "Invalid deviation");

   end Test_Stats_Bounded;

end SCI.Stat_Tests;
