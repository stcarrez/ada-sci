-- --------------------------------------------------------------------
--  sci-stats-tests -- Tests for statistics generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package SCI.Stat_Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Util.Tests.Test with null record;

   --  Tests descriptive stats on bounded float values.
   procedure Test_Stats_Bounded (T : in out Test);

end SCI.Stat_Tests;
