-- --------------------------------------------------------------------
--  sci-similarities-tests -- Tests for similarities generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Util.Test_Caller;
with Util.Strings.Sets;
with SCI.Numbers;
with SCI.Similarities.Indefinite_Hashed_Sets;
package body SCI.Similarities.Tests is

   package String_Similarities is
     new SCI.Similarities.Indefinite_Hashed_Sets (Util.Strings.Sets,
                                                  SCI.Numbers.Float_Conversion);

   package Caller is new Util.Test_Caller (Test, "SCI.Similarities");

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite) is
   begin
      Caller.Add_Test (Suite, "Test Similarities.Jaccard",
                       Test_Jaccard'Access);
      Caller.Add_Test (Suite, "Test Similarities.Sorensen_Dice",
                       Test_Sorensen_Dice'Access);
      Caller.Add_Test (Suite, "Test Similarities.Tversky",
                       Test_Tversky'Access);
   end Add_Tests;

   procedure Test_Jaccard (T : in out Test) is
      Set1 : Util.Strings.Sets.Set;
      Set2 : Util.Strings.Sets.Set;
      Val  : Float;
   begin
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Identical set: " & Val'Image);
      T.Assert (Val = 1.0, "Expecting 1.0, value=" & Val'Image);

      Set1.Insert ("item1");
      Set2.Insert ("item2");
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Not identical set: " & Val'Image);
      T.Assert (Val = 0.0, "Expecting 0.0, value=" & Val'Image);

      Set1.Insert ("item2");
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.5, "Expecting 0.5, value=" & Val'Image);

      Set2.Insert ("item3");
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val >= 0.333 and then Val <= 0.3334,
                "Expecting 0.333, value=" & Val'Image);

      Set2.Insert ("item4");
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.25, "Expecting 0.25, value=" & Val'Image);

      Set1.Insert ("item5");
      Set2.Insert ("item5");
      Val := String_Similarities.Jaccard (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.4, "Expecting 0.4, value=" & Val'Image);

   end Test_Jaccard;

   procedure Test_Sorensen_Dice (T : in out Test) is
      Set1 : Util.Strings.Sets.Set;
      Set2 : Util.Strings.Sets.Set;
      Val  : Float;
   begin
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Identical set: " & Val'Image);
      T.Assert (Val = 1.0, "Expecting 1.0, value=" & Val'Image);

      Set1.Insert ("item1");
      Set2.Insert ("item2");
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Not identical set: " & Val'Image);
      T.Assert (Val = 0.0, "Expecting 0.0, value=" & Val'Image);

      Set1.Insert ("item2");
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val > 0.666 and then Val <= 0.667,
                "Expecting 0.666, value=" & Val'Image);

      Set2.Insert ("item3");
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.5,
                "Expecting 0.5, value=" & Val'Image);

      Set2.Insert ("item4");
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.4, "Expecting 0.4, value=" & Val'Image);

      Set1.Insert ("item5");
      Set2.Insert ("item5");
      Val := String_Similarities.Sorensen_Dice (Set1, Set2);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val > 0.571 and then Val < 0.572,
                "Expecting 0.571, value=" & Val'Image);

   end Test_Sorensen_Dice;

   procedure Test_Tversky (T : in out Test) is
      Set1 : Util.Strings.Sets.Set;
      Set2 : Util.Strings.Sets.Set;
      Val  : Float;
   begin
      --  Note: Tversky (Set1, Set2, 0.5, 0.5) = Sorensen_Dice (Set1, Set2);
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Identical set: " & Val'Image);
      T.Assert (Val = 1.0, "Expecting 1.0, value=" & Val'Image);

      Set1.Insert ("item1");
      Set2.Insert ("item2");
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Not identical set: " & Val'Image);
      T.Assert (Val = 0.0, "Expecting 0.0, value=" & Val'Image);

      Set1.Insert ("item2");
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val > 0.666 and then Val <= 0.667,
                "Expecting 0.666, value=" & Val'Image);

      Set2.Insert ("item3");
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.5,
                "Expecting 0.5, value=" & Val'Image);

      Set2.Insert ("item4");
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val = 0.4, "Expecting 0.4, value=" & Val'Image);

      Set1.Insert ("item5");
      Set2.Insert ("item5");
      Val := String_Similarities.Tversky (Set1, Set2, 0.5, 0.5);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val > 0.571 and then Val < 0.572,
                "Expecting 0.571, value=" & Val'Image);

      Val := String_Similarities.Tversky (Set1, Set2, 0.25, 0.75);
      Ada.Text_IO.Put_Line ("Almost identical set: " & Val'Image);
      T.Assert (Val > 0.533 and then Val < 0.534,
                "Expecting 0.533, value=" & Val'Image);

   end Test_Tversky;

end SCI.Similarities.Tests;
