-- --------------------------------------------------------------------
--  sci-similarities-tests -- Tests for similarities generic package
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

with Util.Tests;
package SCI.Similarities.Tests is

   procedure Add_Tests (Suite : in Util.Tests.Access_Test_Suite);

   type Test is new Util.Tests.Test with null record;

   procedure Test_Jaccard (T : in out Test);
   procedure Test_Sorensen_Dice (T : in out Test);
   procedure Test_Tversky (T : in out Test);

end SCI.Similarities.Tests;
