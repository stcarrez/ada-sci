MAKE_ARGS=

ifeq ($(BUILD),coverage)
MAKE_ARGS=-- -XSCIADA_BUILD=coverage
endif

ifeq ($(BUILD),debug)
MAKE_ARGS=-- -XSCIADA_BUILD=debug
endif

build:
	alr build $(MAKE_ARGS)

clean:
	alr clean
	rm -rf obj lib regtests/bin

test:
	cd regtests && alr build $(MAKE_ARGS)
	./regtests/bin/sci-harness

examples:
	cd examples && alr build $(MAKE_ARGS)

SCI_DOC= \
  title.md \
  pagebreak.tex \
  index.md \
  pagebreak.tex \
  Installation.md \
  pagebreak.tex \
  SCI_Statistics.md \
  pagebreak.tex \
  SCI_Correlations.md \
  pagebreak.tex \
  SCI_Occurrences.md \
  pagebreak.tex \
  SCI_Sparse.md \
  pagebreak.tex \
  SCI_Vectorizers.md \
  pagebreak.tex \
  SCI_Similarities.md

DYNAMO=dynamo
PANDOC=pandoc
DOC_OPTIONS=-f markdown --listings --number-sections --toc
HTML_OPTIONS=-f markdown --listings --number-sections --toc --css pandoc.css

doc:: docs/sci-book.pdf docs/sci-book.html

docs/sci-book.pdf: force
	$(DYNAMO) build-doc -pandoc docs
	cd docs && $(PANDOC) $(DOC_OPTIONS) -o sci-book.pdf --template=./eisvogel.tex $(SCI_DOC)

docs/sci-book.html: docs/sci-book.pdf
	cd docs && $(PANDOC) $(HTML_OPTIONS) -o sci-book.html $(SCI_DOC)

force:

.PHONY: examples
