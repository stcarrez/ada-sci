---
title: "SCI Ada Programmer's Guide"
author: [Stephane Carrez]
date: 2023-12-30
subject: "Ada Collection of Scientific Operations"
tags: [Ada, SCI, Scientific, Correlations, Statistics, Similarities, Occurrences]
titlepage: true
titlepage-color: 06386e
titlepage-text-color: FFFFFF
titlepage-rule-color: FFFFFF
titlepage-rule-height: 1
...
