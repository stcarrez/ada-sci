
# Similarities
The `SCI.Similarities` package provides several algorithms to compute
the similarities between two data sets.  The data sets can contain any
types as long as they are not limited.

