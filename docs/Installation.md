# Installation

This chapter explains how to build and install the library.

In your project, use Alire `with` command to add the `sciada` dependency to your project:

```
alr index --update-all
alr with sciada
```

To use the library in an Ada project, add the following line at the beginning of your
GNAT project file:

```
with "sciada";
```

