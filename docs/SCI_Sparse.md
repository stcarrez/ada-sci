# Sparse arrays
A sparse array or matrix is a special array where most elements contain
a default value, in most cases 0.  To efficiently represent the sparse
array, it uses a data structure where it only records cells with non
default values.

## Coordinate list sparse arrays
The `SCI.Sparse.COO_Arrays` implements a sparse arrays where cells are
recorded using `(row, column, value)` tuples.  The cells are ordered
in an `Ordered_Maps` where the key is the `[row, column]` index and
the map contains the value.  Non empty cells are sorted by rows and
then columns to provide an efficient lookup and multiple-rows computations.

