# Correlations
To use the `Correlations` package, it is first necessary to instantiate
the `SCI.Correlations` package with a `Float_Type` used for the internal
computation.  This floating point type must have enough digits and allow
enough value ranges for proper statistics calculations.

```Ada
 package Double_Correlations is new SCI.Correlations (Double);
```

## Pearson
The `SCI.Correlations.Pearson` package provides operations to compute the
Pearson correlation.  The package must be instantiated with a `Value_Type`
and it then provides several generic functions that must be instantiated
to better fit the data model layout.
For example:

```Ada
 type MyFloat is new Float range 0.0 .. 1_000.0;
 package MyFloat_Pearson is
    new Double_Correlations.Pearson (Value_Type => MyFloat);
```

Several generic function are provided and are ready to be instantiated

* the `Correlation` generic function computes the correlation against two
  arrays of `Value_Type`.
* the `Matrix_Correlation` generic function uses a two dimension array
  and computes the correlation between two columns on a row subset.
* the `Model_Correlation` generic function provides the most versatile
  data model and it is necessary to provide a `Get` function that
  retrieves one value at a time.
