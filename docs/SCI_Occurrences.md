# Occurrences
The `SCI.Occurrences` children packages provide generic packages to
compute occurrence of items.

## Array based items
To find occurence of strings, instantiate with:

```Ada
 package String_Occurrence is
    new SCI.Occurrences.Arrays (Element_Type => Character,
                                Index_Type   => Positive,
                                Array_Type   => String,
                                Occurrence_Type => Natural);
```

## Finite Ordered Sets
The `SCI.Occurrences.Finites` package provides a ordered set where
each element added is associated with an `Occurrence_Type`.  A simple
occurrence of words can be created with:

```Ada
 package Word_Occurrences is
    new SCI.Occurrences.Finites
       (Element_Type => Ada.Strings.Unbounded.Unbounded_String,
        Occurrence_Type => Natural);
```

When items are added to the set, the `Occurrence_Type` values are
merged with existing values.  This is done by the generic `Add`
procedure which must be instantiated with a `+` function that
implements such merge.

```Ada
 procedure Add is new Word_Occurrences.Add;
```

Operations of the package are used in several steps.  In a first
step, the items are collected and added to the set by using the
`Add` procedure.  Once all items are added, you will use:

* the `Get` function can be used to retrieve the occurence value
  associated with an item.
* the `List` procedure to extract a list of items and sort them
  according to a comparison function that you provide for the
  instantiation of the `List` procedure.
* the `Sum` and `Longest` procedures are helper functions that
  operate on the result produced by the `List` procedure.
