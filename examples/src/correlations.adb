-- --------------------------------------------------------------------
--  correlations -- read a CSV file and report correlations between columns
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;
with SCI.Correlations.Pearson;
with Util.Strings;
with Util.Serialize.IO.CSV;
with Util.Serialize.Mappers;
procedure Correlations is
   package AC renames Ada.Command_Line;

   use Util.Serialize.IO.CSV;

   subtype Row_Type is Util.Serialize.IO.CSV.Row_Type range 1 .. 10_000;
   subtype Column_Type is Util.Serialize.IO.CSV.Column_Type range 1 .. 10;

   function Image (R : Column_Type)
                   return String is (Util.Strings.Image (Natural (R)));

   type Model_Values is array (Row_Type range <>, Column_Type range <>) of Float;

   package Float_Correlations is
     new SCI.Correlations (Float);
   package Pearson_Correlations is
     new Float_Correlations.Pearson (Value_Type => Float);
   function Correlate is
     new Pearson_Correlations.Matrix_Correlation (Matrix_Type => Model_Values,
                                                  Row_Type    => Row_Type,
                                                  Column_Type => Column_Type);

   procedure Print_Correlations;

   Prev_Row : Util.Serialize.IO.CSV.Row_Type := 0;
   Last_Col : Column_Type := 1;
   Values   : Model_Values (1 .. Row_Type'Last, 1 .. Column_Type'Last);

   type CSV_Parser is new Util.Serialize.IO.CSV.Parser with null record;

   overriding
   procedure Set_Cell (Parser : in out CSV_Parser;
                       Value  : in String;
                       Row    : in Util.Serialize.IO.CSV.Row_Type;
                       Column : in Util.Serialize.IO.CSV.Column_Type);

   overriding
   procedure Set_Cell (Parser : in out CSV_Parser;
                       Value  : in String;
                       Row    : in Util.Serialize.IO.CSV.Row_Type;
                       Column : in Util.Serialize.IO.CSV.Column_Type) is
      pragma Unreferenced (Parser);
   begin
      if Row < Values'Last (1) and then Column < Values'Last (2) then
         if Prev_Row /= Row then
            Prev_Row := Row;
         end if;
         if Last_Col < Column then
            Last_Col := Column;
         end if;
         declare
            Val : constant Float := Float'Value (Value);
         begin
            Values (Row, Column) := Val;
         end;
      end if;
   exception
      when others =>
         null;
   end Set_Cell;

   procedure Print_Correlations is
      type Rho_Type is delta 0.001 range -1.0 .. 1.0;
      Max_Rho : Rho_Type := 0.0;
      Max_C1, Max_C2 : Column_Type := 1;
   begin
      for I in 1 .. Last_Col loop
         for J in I + 1 .. Last_Col loop
            declare
               Rho : constant Rho_Type := Rho_Type (Correlate (Values, 1, Prev_Row, I, J));
            begin
               if Rho > Max_Rho then
                  Max_Rho := Rho;
                  Max_C1 := I;
                  Max_C2 := J;
               end if;
            end;
         end loop;
      end loop;
      Ada.Text_IO.Put_Line ("Columns with max correlation: "
                            & Image (Max_C1) & " and " & Image (Max_C2)
                            & " with rho: " & Max_Rho'Image);
   end Print_Correlations;

   Count  : constant Natural := Ada.Command_Line.Argument_Count;
   Parser : CSV_Parser;
begin
   for I in 1 .. Count loop
      declare
         Path   : constant String := AC.Argument (I);
         Mapper : Util.Serialize.Mappers.Processing;
      begin
         if Ada.Directories.Exists (Path) then
            Prev_Row := 0;
            Last_Col := 1;
            Parser.Parse (Path, Mapper);
            Print_Correlations;
         else
            Ada.Text_IO.Put (Path);
            Ada.Text_IO.Put_Line (": is not a file");
         end if;
      end;
   end loop;
end Correlations;
