-- --------------------------------------------------------------------
--  helpers -- helper package for examples
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
package Helpers is

   function Is_Word (C : Character)
      return Boolean is (C in 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9');

   function Skip_Spaces (Line : String;
                         From : Natural) return Natural;

   function Skip_Word (Line : String;
                       From : Natural) return Natural;

   procedure Read_File (Path    : String;
                        Process : not null access procedure (Word : String));

end Helpers;
