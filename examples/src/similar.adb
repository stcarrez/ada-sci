-- --------------------------------------------------------------------
--  similar -- indicate similarities between files
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;
with SCI.Numbers;
with SCI.Similarities.Indefinite_Hashed_Sets;
with SCI.Similarities.COO_Arrays;
with SCI.Sparse.COO_Arrays;
with SCI.Vectorizers.Indefinite_Counters;
with SCI.Vectorizers.Transformers;
with Util.Strings.Sets;
with Helpers;
procedure Similar is
   package AC renames Ada.Command_Line;

   type Measure is delta 0.001 range 0.0 .. 1.0;

   --  ??? Mul and Div seem necessary as "*" and "/" fail to instantiate
   function Mul (Left, Right : Measure) return Measure is (Left * Right);
   function Div (Left, Right : Measure) return Measure is (Left / Right);
   function From_Float (Value : Float) return Measure is (Measure (Value));
   function From_Integer (Value : Integer) return Measure is (Measure (Value));
   function Increment (Value : in Natural) return Natural;

   package Measure_Number is
     new SCI.Numbers.Number (Measure, "*" => Mul, "/" => Div);
   package Measure_Conversion is
     new SCI.Numbers.Conversion (Measure_Number);
   package String_Similarities is
      new SCI.Similarities.Indefinite_Hashed_Sets (Util.Strings.Sets, Measure_Conversion);

   package Arrays is
     new SCI.Sparse.COO_Arrays (Row_Type      => Positive,
                                Column_Type   => Positive,
                                Value_Type    => Natural,
                                Default_Value => 0);

   package Counters is
     new SCI.Vectorizers.Indefinite_Counters (Token_Type => String,
                                              Arrays     => Arrays);
   function To_Float (Value : Natural) return Float is (Float (Value));
   package Arrays_Similarities is
     new SCI.Similarities.COO_Arrays (Arrays      => Arrays,
                                      Conversions => Measure_Conversion,
                                      To_Float    => To_Float);

   package Float_Frequencies is
     new SCI.Vectorizers.Transformers (Frequency_Type => Float,
                                       Arrays => Arrays,
                                       Convert => To_Float);
   function To_Float (Value : Float) return Float is (Value);
   package Frequency_Similarities is
     new SCI.Similarities.COO_Arrays (Arrays      => Float_Frequencies.Frequency_Arrays,
                                      Conversions => Measure_Conversion,
                                      To_Float    => To_Float);

   function Increment (Value : in Natural) return Natural is
   begin
      return Value + 1;
   end Increment;

   Count : constant Natural := AC.Argument_Count;

   type File_Word_Array is array (1 .. Count) of Util.Strings.Sets.Set;

   File_Words : File_Word_Array;
   Info : Counters.Vectorizer_Type;
   Freq : Float_Frequencies.Frequency_Arrays.Array_Type;
begin
   Info.Counters.Default := 0;
   for I in 1 .. Count loop
      declare
         procedure Collect (Word : String);

         Path : constant String := AC.Argument (I);

         procedure Collect (Word : String) is
         begin
            File_Words (I).Include (Word);
            Counters.Add_Token (Info, I, Word, Increment'Access);
         end Collect;
      begin
         if Ada.Directories.Exists (Path) then
            Helpers.Read_File (Path, Collect'Access);
         else
            Ada.Text_IO.Put (Path);
            Ada.Text_IO.Put_Line (": is not a file");
         end if;
      end;
   end loop;
   Float_Frequencies.TIDF (From => Info.Counters, Into => Freq);
   for I in 2 .. Count loop
      declare
         JA : constant Measure
           := String_Similarities.Jaccard (File_Words (1),
                                           File_Words (I));
         SD : constant Measure
           := String_Similarities.Sorensen_Dice (File_Words (1),
                                                 File_Words (I));
         TV : constant Measure
           := String_Similarities.Tversky (File_Words (1),
                                           File_Words (I),
                                           0.25, 0.75);
         CO : constant Measure := Arrays_Similarities.Cosine (Info.Counters, 1, Info.Counters, I);
         CF : constant Measure := Frequency_Similarities.Cosine (Freq, 1, Freq, I);
      begin
         Ada.Text_IO.Put_Line (AC.Argument (1) & " - "
                               & AC.Argument (I) & ": Jaccard:"
                               & JA'Image & " Sorensen Dice:" & SD'Image
                               & " Tversky:" & TV 'Image
                               & " Cosine:" & CO'Image
                               & " TIDF cosine:" & CF'Image);
      end;
   end loop;
end Similar;
