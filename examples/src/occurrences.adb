-- --------------------------------------------------------------------
--  occurrences -- report occurrence of words in files
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Text_IO;
with Ada.Command_Line;
with Ada.Directories;
with SCI.Occurrences.Arrays;
with Helpers;
procedure Occurrences is
   package AC renames Ada.Command_Line;

   package Word_Occurrences is
     new SCI.Occurrences.Arrays (Element_Type => Character,
                                 Index_Type => Positive,
                                 Array_Type => String,
                                 Occurrence_Type => Natural);

   procedure List_Occurrences is new Word_Occurrences.List;
   procedure Add is new Word_Occurrences.Add;
   function Sum is new Word_Occurrences.Sum;

   procedure Collect (Token : in String);

   Words : Word_Occurrences.Sets.Set;

   procedure Collect (Token : in String) is
   begin
      Add (Words, Token, 1);
   end Collect;

begin
   for I in 1 .. AC.Argument_Count loop
      declare
         Path : constant String := AC.Argument (I);
      begin
         if Ada.Directories.Exists (Path) then
            Helpers.Read_File (Path, Collect'Access);
         else
            Ada.Text_IO.Put (Path);
            Ada.Text_IO.Put_Line (": is not a file");
         end if;
      end;
   end loop;

   declare
      List  : Word_Occurrences.Vector;
      Total : Natural;
   begin
      List_Occurrences (Words, List);
      Total := Sum (List, 0);
      Ada.Text_IO.Put_Line ("Total:" & Total'Image);
      for Item of List loop
         Ada.Text_IO.Put (Item.Element);
         Ada.Text_IO.Put (" ");
         Ada.Text_IO.Put_Line (Item.Occurrence'Image);
      end loop;
   end;
end Occurrences;
