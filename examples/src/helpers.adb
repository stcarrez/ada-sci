-- --------------------------------------------------------------------
--  helpers -- helper package for examples
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Util.Files;
package body Helpers is

   function Skip_Spaces (Line : String;
                         From : Natural) return Natural is
      Pos : Natural := From;
   begin
      while Pos <= Line'Last and then not Is_Word (Line (Pos)) loop
         Pos := Pos + 1;
      end loop;
      return Pos;
   end Skip_Spaces;

   function Skip_Word (Line : String;
                       From : Natural) return Natural is
      Pos : Natural := From;
   begin
      while Pos <= Line'Last and then Is_Word (Line (Pos)) loop
         Pos := Pos + 1;
      end loop;
      return Pos;
   end Skip_Word;

   procedure Read_File (Path    : String;
                        Process : not null access procedure (Word : String)) is
      procedure Read_Line (Line : String);

      procedure Read_Line (Line : String) is
         Pos   : Natural := Line'First;
         First : Natural := Pos;
      begin
         while Pos <= Line'Last loop
            First := Skip_Spaces (Line, Pos);
            exit when First > Line'Last;
            Pos := Skip_Word (Line, First);
            Process (Line (First .. Pos - 1));
         end loop;
      end Read_Line;
   begin
      Util.Files.Read_File (Path, Read_Line'Access);
   end Read_File;

end Helpers;
