-- --------------------------------------------------------------------
--  sci-vectorizers -- transform lists of tokens to vectors
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  = Vectorizers =
--  A vectorizer transforms a list of tokens to a vector where we can
--  make mathematical computations.  The vectorizers are generic packages
--  that must be instantiated with a sparse array package which describes
--  the final vector.  The first step is to instantiate a sparse array
--  with a `Row_Type` and `Column_Type` which represent indices of the
--  vector and a `Value_Type` which represents value of the cell.  For example,
--  a simple counter vector can be declared as follows:
--
--    package Counter_Arrays is
--       new SCI.Sparse.COO_Arrays (Row_Type    => Positive,
--                                  Column_Type => Positive,
--                                  Value_Type  => Natural);
--
--  The sparse array only records cells which have values.  The `Row_Type` can
--  be used to represent a document and the `Column_Type` refers to the
--  occurrence of the token in that document.
--
--  @include sci-vectorizers-counters.ads
--  @include sci-vectorizers-indefinite_counters.ads
--  @include sci-vectorizers-transformers.ads
package SCI.Vectorizers with Pure is

end SCI.Vectorizers;
