-- --------------------------------------------------------------------
--  sci-vectorizers-counters -- count occurrence of tokens for the vector
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Vectorizers.Counters is

   --  ------------------------------
   --  Record a new token for the given row.  The token is searched in
   --  the tokens map which gives the associated column in the sparse array.
   --  If the token is not found, the token is inserted and integrated as
   --  a new column in the sparse array.  The cell at [row, column] is
   --  then incremented.
   --  ------------------------------
   procedure Add_Token (Into  : in out Vectorizer_Type;
                        Row   : in Row_Type;
                        Token : in Token_Type;
                        Increment : not null access
                          function (Value : in Value_Type) return Value_Type) is
      Pos : constant Token_Maps.Cursor := Into.Tokens.Find (Token);
   begin
      if Token_Maps.Has_Element (Pos) then
         Arrays.Update (Into.Counters, Row, Token_Maps.Element (Pos), Increment);
      else
         Into.Tokens.Insert (Token, Into.Last_Column);
         Arrays.Update (Into.Counters, Row, Into.Last_Column, Increment);
         Into.Last_Column := Column_Type'Succ (Into.Last_Column);
      end if;
   end Add_Token;

end SCI.Vectorizers.Counters;
