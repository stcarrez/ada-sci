-- --------------------------------------------------------------------
--  sci-statistics -- computation arround statistics
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Numerics.Generic_Elementary_Functions;

--  = Statistics =
--  To use the `Statistics` package, it is first necessary to instantiate
--  the `SCI.Statistics` package with a `Float_Type` used for the internal
--  computation.  This floating point type must have enough digits and allow
--  enough value ranges for proper statistics calculations.
--
--     package Double_Statistics is new SCI.Statistics (Double);
--
--  The `SCI.Statistics.Descriptive` package provides operations used by
--  descriptive statistics: min, max, mean, sum, deviation.  It must be
--  instantiated with the target floating point type that describe values,
--  an index type and an array type that describe the array of values.
--  For example:
--
--     type MyFloat is new Float range 0.0 .. 1_000.0;
--     type MyFloat_Array is array (Positive range <>) of MyFloat;
--     package MyFloat_Statistics is
--        new Double_Statistics.Descriptive (Value_Type => MyFloat,
--                                           Index_Type => Positive,
--                                           Array_Type => MyFloat_Array);
--
generic
   type Float_Type is digits <>;
package SCI.Statistics with Pure is

   package Functions is
      new Ada.Numerics.Generic_Elementary_Functions (Float_Type);

end SCI.Statistics;
