-- --------------------------------------------------------------------
--  sci-occurrences-arrays -- identify occurrences of array based items
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  == Array based items ==
--  To find occurence of strings, instantiate with:
--
--     package String_Occurrence is
--        new SCI.Occurrences.Arrays (Element_Type => Character,
--                                    Index_Type   => Positive,
--                                    Array_Type   => String,
--                                    Occurrence_Type => Natural);
with Ada.Containers.Indefinite_Ordered_Sets;
with Ada.Containers.Indefinite_Vectors;
generic
   type Element_Type is private;
   type Index_Type is (<>);
   type Array_Type is array (Index_Type range <>) of Element_Type;
   type Occurrence_Type is private;
   with function "<" (Left, Right : Array_Type) return Boolean is <>;
package SCI.Occurrences.Arrays is

   type Occurrence (First, Last : Index_Type) is record
      Element    : Array_Type (First .. Last);
      Occurrence : Occurrence_Type;
   end record;

   --  Compare the two occurrence only on their name.
   function "<" (Left, Right : Occurrence)
                 return Boolean is (Left.Element < Right.Element);
   function Same (Left, Right : Occurrence)
                  return Boolean is (Left.Element = Right.Element);

   package Vectors is
     new Ada.Containers.Indefinite_Vectors (Positive, Occurrence);
   subtype Vector is Vectors.Vector;

   package Sets is
     new Ada.Containers.Indefinite_Ordered_Sets (Occurrence, "<", Same);
   subtype Set is Sets.Set;

   --  Add the item in the set.  If the item already exists, add the value
   --  to the current count.
   generic
      with function "+" (Left, Right : Occurrence_Type) return Occurrence_Type is <>;
   procedure Add (Set   : in out Sets.Set;
                  Item  : in Array_Type;
                  Value : in Occurrence_Type);

   --  Get the list of occurrence items sorted on the occurence value.
   generic
      with function "<" (Left, Right : Occurrence_Type) return Boolean is <>;
   procedure List (Set  : in Sets.Set;
                   Into : in out Vectors.Vector);

   --  Return the longest item in the list.
   generic
      with function Length (Item : Array_Type) return Natural is <>;
   function Longest (From : in Vectors.Vector) return Natural;

   --  Compute the sum of every element count.
   generic
      with function "+" (Left, Right : Occurrence_Type) return Occurrence_Type is <>;
   function Sum (From    : in Vectors.Vector;
                 Initial : in Occurrence_Type) return Occurrence_Type;

end SCI.Occurrences.Arrays;
