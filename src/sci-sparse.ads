-- --------------------------------------------------------------------
--  sci-sparse -- sparse arrays and matrix
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  = Sparse arrays =
--  A sparse array or matrix is a special array where most elements contain
--  a default value, in most cases 0.  To efficiently represent the sparse
--  array, it uses a data structure where it only records cells with non
--  default values.
--
--  @include sci-sparse-coo_arrays.ads
package SCI.Sparse with Pure is

end SCI.Sparse;
