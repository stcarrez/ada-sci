-- --------------------------------------------------------------------
--  sci-similarities-coo_arrays -- similarities with sparse arrays
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Numerics.Generic_Elementary_Functions;

package body SCI.Similarities.COO_Arrays is

   package Functions is
      new Ada.Numerics.Generic_Elementary_Functions (Float);

   function "*" (Left, Right : Arrays.Value_Type) return Float is
   begin
      return To_Float (Left) * To_Float (Right);
   end "*";

   function Sum_Square (Left  : Float;
                        Right : Arrays.Value_Type) return Float is
      V : constant Float := To_Float (Right);
   begin
      return Left + V * V;
   end Sum_Square;

   function Sqrt_Square (From : in Arrays.Array_Type;
                         Row  : in Row_Type) return Float is
      Sq_Row : constant Float := Square (From, Row, 0.0);
   begin
      return Functions.Sqrt (Sq_Row);
   end Sqrt_Square;

   function Cosine (A      : in Arrays.Array_Type;
                    A_Row  : in Row_Type;
                    B      : in Arrays.Array_Type;
                    B_Row  : in Row_Type;
                    Sqrt_A : in Float;
                    Sqrt_B : in Float) return Value_Type is
      AB : constant Float := Multiply (A, A_Row, B, B_Row);
   begin
      return Conversions.From_Float (AB / (Sqrt_A * Sqrt_B));
   end Cosine;

   function Cosine (A      : in Arrays.Array_Type;
                    A_Row  : in Row_Type;
                    B      : in Arrays.Array_Type;
                    B_Row  : in Row_Type;
                    Sqrt_B : in Float) return Value_Type is
      AB     : constant Float := Multiply (A, A_Row, B, B_Row);
      Sqrt_A : constant Float := Sqrt_Square (A, A_Row);
   begin
      return Conversions.From_Float (AB / (Sqrt_A * Sqrt_B));
   end Cosine;

   function Cosine (A     : in Arrays.Array_Type;
                    A_Row : in Row_Type;
                    B     : in Arrays.Array_Type;
                    B_Row : in Row_Type) return Value_Type is
      AB     : constant Float := Multiply (A, A_Row, B, B_Row);
      Sqrt_A : constant Float := Sqrt_Square (A, A_Row);
      Sqrt_B : constant Float := Sqrt_Square (B, B_Row);
   begin
      return Conversions.From_Float (AB / (Sqrt_A * Sqrt_B));
   end Cosine;

end SCI.Similarities.COO_Arrays;
