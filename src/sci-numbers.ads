-- --------------------------------------------------------------------
--  sci-numbers -- float and fixed point abstractions
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package SCI.Numbers with Pure is

   --  The generic Number package is used to declare and import operators for
   --  another package instantiation, hence these operators are not used here.
   pragma Warnings (Off, "*is not referenced*");
   generic
      type Element_Type is private;
      with function "+" (Left, Right : Element_Type) return Element_Type is <>;
      with function "-" (Left, Right : Element_Type) return Element_Type is <>;
      with function "*" (Left, Right : Element_Type) return Element_Type is <>;
      with function "/" (Left, Right : Element_Type) return Element_Type is <>;
   package Number is
   end Number;

   generic
      with package Number_Types is new Number (<>);
      with function From_Integer (Value : in Integer) return Number_Types.Element_Type is <>;
      with function From_Float (Value : in Float) return Number_Types.Element_Type is <>;
   package Conversion is
      subtype Element is Number_Types.Element_Type;
      function "+" (Left, Right : Element) return Element renames Number_Types."+";
      function "-" (Left, Right : Element) return Element renames Number_Types."-";
      function "*" (Left, Right : Element) return Element renames Number_Types."*";
      function "/" (Left, Right : Element) return Element renames Number_Types."/";
   end Conversion;

   package Float_Number is new Number (Float);

   function From_Integer (Value : in Integer) return Float is (Float (Value));
   function From_Float (Value : in Float) return Float is (Value);

   package Float_Conversion is
     new Conversion (Number_Types => Float_Number,
                     From_Integer => From_Integer,
                     From_Float   => From_Float);

end SCI.Numbers;
