-- --------------------------------------------------------------------
--  sci-correlations-pearson -- compute Pearson correlation between values
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  == Pearson ==
--  The `SCI.Correlations.Pearson` package provides operations to compute the
--  Pearson correlation.  The package must be instantiated with a `Value_Type`
--  and it then provides several generic functions that must be instantiated
--  to better fit the data model layout.
--  For example:
--
--     type MyFloat is new Float range 0.0 .. 1_000.0;
--     package MyFloat_Pearson is
--        new Double_Correlations.Pearson (Value_Type => MyFloat);
--
--  Several generic function are provided and are ready to be instantiated
--
--  * the `Correlation` generic function computes the correlation against two
--    arrays of `Value_Type`.
--  * the `Matrix_Correlation` generic function uses a two dimension array
--    and computes the correlation between two columns on a row subset.
--  * the `Model_Correlation` generic function provides the most versatile
--    data model and it is necessary to provide a `Get` function that
--    retrieves one value at a time.
generic
   type Value_Type is digits <>;
package SCI.Correlations.Pearson with Pure is

   --  Compute Pearson correlation between two arrays of same length.
   generic
      type Index_Type is (<>);
      type Array_Type is array (Index_Type range <>) of Value_Type;
   function Correlation (X, Y : in Array_Type) return Float_Type
     with Pre => X'Length = Y'Length
     and then X'First = Y'First;

   --  Compute Pearson correlation for a matrix between two columns C1 and C2
   --  and limited to the rows between From and To inclusive.
   generic
      type Row_Type is (<>);
      type Column_Type is (<>);
      type Matrix_Type is array (Row_Type range <>,
                                 Column_Type range <>) of Value_Type;
   function Matrix_Correlation (M : in Matrix_Type;
                                From, To : in Row_Type;
                                C1, C2 : in Column_Type) return Float_Type;

   --  Compute Pearson correlation for a generic model between two columns
   --  C1 and C2 and limited to the rows between From and To inclusive.
   --  Values are retrieved by calling the `Get` function with the current
   --  row and either C1 or C2.
   generic
      type Model_Type is limited private;
      type Row_Type is (<>);
      type Column_Type is (<>);
      with function Get (Model : in Model_Type;
                         Row   : in Row_Type;
                         Col   : in Column_Type) return Value_Type;
   function Model_Correlation (M : in Model_Type;
                               From, To : in Row_Type;
                               C1, C2 : in Column_Type) return Float_Type;

   function Compute (Count : Natural;
                     Sum_X, Sum_Y, Sq_X, Sq_Y, XY : Float_Type) return Float_Type;

end SCI.Correlations.Pearson;
