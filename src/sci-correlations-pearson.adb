-- --------------------------------------------------------------------
--  sci-correlations-pearson -- compute Pearson correlation between values
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Correlations.Pearson with Pure is

   function Compute (Count : Natural;
                     Sum_X, Sum_Y, Sq_X, Sq_Y, XY : Float_Type) return Float_Type is
      N   : constant Float_Type := Float_Type (Count);
      Num : constant Float_Type := N * XY - Sum_X * Sum_Y;
      Dx  : constant Float_Type := N * Sq_X - Sum_X * Sum_X;
      Dy  : constant Float_Type := N * Sq_Y - Sum_Y * Sum_Y;
   begin
      return Num / (Functions.Sqrt (Dx) * Functions.Sqrt (Dy));
   end Compute;

   function Correlation (X, Y : in Array_Type) return Float_Type is
      Sum_X : Float_Type := 0.0;
      Sum_Y : Float_Type := 0.0;
      Sq_X  : Float_Type := 0.0;
      Sq_Y  : Float_Type := 0.0;
      XY    : Float_Type := 0.0;
   begin
      --  Compute in one pass the values.
      for I in X'Range loop
         declare
            Vx : constant Float_Type := Float_Type (X (I));
            Vy : constant Float_Type := Float_Type (Y (I));
         begin
            Sum_X := Sum_X + Vx;
            Sum_Y := Sum_Y + Vy;
            Sq_X := Sq_X + Vx * Vx;
            Sq_Y := Sq_Y + Vy * Vy;
            XY := XY + Vx * Vy;
         end;
      end loop;
      return Compute (X'Length, Sum_X, Sum_Y, Sq_X, Sq_Y, XY);
   end Correlation;

   function Matrix_Correlation (M : in Matrix_Type;
                                From, To : in Row_Type;
                                C1, C2 : in Column_Type) return Float_Type is
      Sum_X : Float_Type := 0.0;
      Sum_Y : Float_Type := 0.0;
      Sq_X  : Float_Type := 0.0;
      Sq_Y  : Float_Type := 0.0;
      XY    : Float_Type := 0.0;
      Count : Natural := 0;
   begin
      --  Compute in one pass the values.
      for I in From .. To loop
         declare
            Vx : constant Float_Type := Float_Type (M (I, C1));
            Vy : constant Float_Type := Float_Type (M (I, C2));
         begin
            Sum_X := Sum_X + Vx;
            Sum_Y := Sum_Y + Vy;
            Sq_X := Sq_X + Vx * Vx;
            Sq_Y := Sq_Y + Vy * Vy;
            XY := XY + Vx * Vy;
            Count := Count + 1;
         end;
      end loop;
      return Compute (Count, Sum_X, Sum_Y, Sq_X, Sq_Y, XY);
   end Matrix_Correlation;

   function Model_Correlation (M : in Model_Type;
                               From, To : in Row_Type;
                               C1, C2 : in Column_Type) return Float_Type is
      Sum_X : Float_Type := 0.0;
      Sum_Y : Float_Type := 0.0;
      Sq_X  : Float_Type := 0.0;
      Sq_Y  : Float_Type := 0.0;
      XY    : Float_Type := 0.0;
      Count : Natural := 0;
   begin
      --  Compute in one pass the values.
      for I in From .. To loop
         declare
            X  : constant Value_Type := Get (M, I, C1);
            Y  : constant Value_Type := Get (M, I, C2);
            Vx : constant Float_Type := Float_Type (X);
            Vy : constant Float_Type := Float_Type (Y);
         begin
            Sum_X := Sum_X + Vx;
            Sum_Y := Sum_Y + Vy;
            Sq_X := Sq_X + Vx * Vx;
            Sq_Y := Sq_Y + Vy * Vy;
            XY := XY + Vx * Vy;
            Count := Count + 1;
         end;
      end loop;
      return Compute (Count, Sum_X, Sum_Y, Sq_X, Sq_Y, XY);
   end Model_Correlation;

end SCI.Correlations.Pearson;
