-- --------------------------------------------------------------------
--  sci-similarities -- compute similarities between sets
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with SCI.Numbers;

--  = Similarities =
--  The `SCI.Similarities` package provides several algorithms to compute
--  the similarities between two data sets.  The data sets can contain any
--  types as long as they are not limited.
package SCI.Similarities with Pure is

   subtype Tversky_Weight is Float range 0.0 .. 1.0;

   --  Compute the Jaccard similarity between the two sets:
   --  (Count1 + Count2 - Union_Count) / Union_Count or 0.0 if Union_Count = 0
   generic
      with package Conversions is new SCI.Numbers.Conversion (<>);
   function Jaccard (Count1, Count2, Union_Count : in Natural)
                     return Conversions.Element;

   --  Compute the Sorensen Dice similarity between the two sets:
   --  2.0 * Inter_Count / (Count1 + Count2) or 0.0 if Count1 + Count2 = 0
   generic
      with package Conversions is new SCI.Numbers.Conversion (<>);
   function Sorensen_Dice (Count1, Count2, Inter_Count : in Natural)
                           return Conversions.Element;

   --  Compute the Tversky index between the two sets:
   --  Inter / (Inter + Alpha * (C1=Set1 / Set2) + Beta * (C2=Set2 / Set1))
   generic
      with package Conversions is new SCI.Numbers.Conversion (<>);
   function Tversky (Inter_Count, C1, C2 : in Natural;
                     Alpha, Beta : in Tversky_Weight) return Conversions.Element;

end SCI.Similarities;
