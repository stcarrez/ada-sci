-- --------------------------------------------------------------------
--  sci-vectorizers-transformers -- apply TF or TIDF transformations
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with SCI.Sparse.COO_Arrays;

--  == Transformers ==
--  The `SCI.Vectorizers.Transformers` transforms a count matrix to a normalized
--  `tf` or `tf-idf` representation.  Tf means term-frequency while tf-idf means
--  term-frequency times inverse document-frequency.  This is a common term
--  weighting scheme in information retrieval, that has also found good use in
--  document classification.
--
--  The `Frequency_Type` defines the floating type to represent the frequency.
--  A `Convert` function must be provided to convert the counter number used
--  by the sparse array into a `Frequency_Type`.  The transformer is then
--  instantiated:
--
--    function To_Float (Value : Natural) return Float is (Float (Value));
--    package Counter_Transformers is
--       new SCI.Vectorizers.Transformers (Frequency_Type => Float,
--                                         Arrays => Counter_Arrays,
--                                         Convert => To_Float);
--
--  Given the counters computed by the `SCI.Vectorizers.Counters` package,
--  the `tf-idf` values are computed as follows:
--
--    F : Counter_Transformers.Frequency_Arrays.Array_Type;
--    ...
--       Counter_Transformers.TIDF (From => V.Counters, Into => F);
generic
   type Frequency_Type is digits <>;
   with package Arrays is new SCI.Sparse.COO_Arrays (<>);
   with function Convert (Value : Arrays.Value_Type) return Frequency_Type;
package SCI.Vectorizers.Transformers is

   subtype Row_Type is Arrays.Row_Type;
   subtype Column_Type is Arrays.Column_Type;

   package Frequency_Arrays is
     new SCI.Sparse.COO_Arrays (Row_Type      => Row_Type,
                                Column_Type   => Column_Type,
                                Value_Type    => Frequency_Type,
                                Default_Value => 0.0);

   type Frequency_Array is array (Column_Type range <>) of Frequency_Type;

   type IDF_Mode is (IDF_Normal, IDF_Smooth, IDF_Probabilistic);

   --  Compute the inverse document frequency as:
   --  when IDF_Normal => idf(t, D) = log(N / n_t)
   --  when IDF_Smooth => idf(t, D) = log(N / 1 + n_t) + 1
   --  when IDF_Probabilistic => idf(t, D) = log(N - n_t / n_t)
   --  where N is the number of documents.
   function IDF (From : in Arrays.Array_Type;
                 Mode : in IDF_Mode := IDF_Smooth) return Frequency_Array with
     Pre => not From.Cells.Is_Empty;

   function IDF (From      : in Arrays.Array_Type;
                 Doc_Count : in Natural;
                 Mode      : in IDF_Mode := IDF_Smooth) return Frequency_Array with
     Pre => not From.Cells.Is_Empty;

   --  Compute the term inverse document frequency as:
   --    tfidf(t,d,D) = tf(t,d) * idf(t, D)
   --  where tf(t,d) indicates the occurrence of the term in the document
   --  (ie, the cell as [row, column] in the From array).
   procedure TIDF (From : in Arrays.Array_Type;
                   Into : in out Frequency_Arrays.Array_Type) with
     Pre => not From.Cells.Is_Empty;

   procedure TIDF (From     : in Arrays.Array_Type;
                   Doc_Freq : in Frequency_Array;
                   Into     : in out Frequency_Arrays.Array_Type) with
     Pre => not From.Cells.Is_Empty;

end SCI.Vectorizers.Transformers;
