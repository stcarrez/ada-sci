-- --------------------------------------------------------------------
--  sci-similarities-indefinite_hashed_sets -- similarities with Indefinite_Hashed_Sets
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Containers.Indefinite_Hashed_Sets;
with SCI.Numbers;
generic
   with package Sets is new Ada.Containers.Indefinite_Hashed_Sets (<>);
   with package Conversions is new SCI.Numbers.Conversion (<>);
package SCI.Similarities.Indefinite_Hashed_Sets is

   subtype Value_Type is Conversions.Element;

   --  Compute the Jaccard similarity between the two sets:
   --  1.0 * (Set1.Length + Set2.Length - Union.Length) / Union.Length
   function Jaccard is new SCI.Similarities.Jaccard (Conversions);
   function Jaccard (Set1, Set2 : in Sets.Set) return Value_Type;

   --  Compute the Sorensen Dice similarity between the two sets:
   --  2.0 * Inter.Length / (Set1.Length + Set2.Length)
   function Sorensen_Dice is new SCI.Similarities.Sorensen_Dice (Conversions);
   function Sorensen_Dice (Set1, Set2 : in Sets.Set) return Value_Type;

   --  Compute the Tversky index between the two sets:
   --  Inter / (Inter + Alpha * (Set1 / Set2) + Beta * (Set2 / Set1))
   function Tversky is new SCI.Similarities.Tversky (Conversions);
   function Tversky (Set1, Set2  : in Sets.Set;
                     Alpha, Beta : in Tversky_Weight) return Value_Type;

end SCI.Similarities.Indefinite_Hashed_Sets;
