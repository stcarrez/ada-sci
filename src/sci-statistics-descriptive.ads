-- --------------------------------------------------------------------
--  sci-statistics-descriptive -- descriptive statistics (mean, ...)
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
generic
   type Value_Type is digits <>;
   type Index_Type is (<>);
   type Array_Type is array (Index_Type range <>) of Value_Type;
package SCI.Statistics.Descriptive with Pure is

   type Stats_Type is record
      Min, Max, Mean         : Value_Type;
      Sum, Sum_Square, IMean : Float_Type := 0.0;
   end record;

   --  Compute min, max, mean, sum, sum_square of values.
   function Compute (Values : in Array_Type) return Stats_Type;

   --  Compute standard deviation of values.
   function Deviation (Values : in Array_Type) return Float_Type;

   --  Compute standard deviation of values (assuming that sum, sum_square
   --  were produced on the same values by Compute).
   function Deviation (Values : in Array_Type;
                       Stats  : in Stats_Type) return Float_Type;

end SCI.Statistics.Descriptive;
