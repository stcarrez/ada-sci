-- --------------------------------------------------------------------
--  sci-statistics-descriptive -- descriptive statistics (mean, ...)
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Statistics.Descriptive is

   function Compute (Values : in Array_Type) return Stats_Type is
      Result : Stats_Type;
   begin
      Result.Min := Value_Type'Last;
      Result.Max := Value_Type'First;
      for Value of Values loop
         if Value > Result.Max then
            Result.Max := Value;
         end if;
         if Value < Result.Min then
            Result.Min := Value;
         end if;
         declare
            V : constant Float_Type := Float_Type (Value);
         begin
            Result.Sum := Result.Sum + V;
            Result.Sum_Square := Result.Sum_Square + V * V;
         end;
      end loop;
      if Values'Length /= 0 then
         Result.IMean := Result.Sum / Float_Type (Values'Length);
         Result.Mean := Value_Type (Result.IMean);
      end if;
      return Result;
   end Compute;

   function Deviation (Values : in Array_Type) return Float_Type is
      Result : constant Stats_Type := Compute (Values);
   begin
      return Deviation (Values, Result);
   end Deviation;

   function Deviation (Values : in Array_Type;
                       Stats  : in Stats_Type) return Float_Type is
      Variance : Float_Type;
   begin
      if Values'Length = 0 then
         return 0.0;
      end if;
      Variance := Stats.Sum_Square / Float_Type (Values'Length);
      Variance := Variance - (Stats.IMean * Stats.IMean);
      return Functions.Sqrt (Variance);
   end Deviation;

end SCI.Statistics.Descriptive;
