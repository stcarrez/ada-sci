-- --------------------------------------------------------------------
--  sci-sparse-coo_arrays -- sparse arrays using COO implementation
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Containers.Ordered_Maps;

--  == Coordinate list sparse arrays ==
--  The `SCI.Sparse.COO_Arrays` implements a sparse arrays where cells are
--  recorded using `(row, column, value)` tuples.  The cells are ordered
--  in an `Ordered_Maps` where the key is the `[row, column]` index and
--  the map contains the value.  Non empty cells are sorted by rows and
--  then columns to provide an efficient lookup and multiple-rows computations.
generic
   type Row_Type is (<>);
   type Column_Type is (<>);
   type Value_Type is private;
   Default_Value : Value_Type;
package SCI.Sparse.COO_Arrays is

   type Index_Type is record
      Row    : Row_Type;
      Column : Column_Type;
   end record;

   function "<" (Left, Right : Index_Type)
                 return Boolean is (Left.Row < Right.Row
                                    or else (Left.Row = Right.Row
                                      and then Left.Column < Right.Column));

   package Maps is
     new Ada.Containers.Ordered_Maps (Key_Type => Index_Type,
                                      Element_Type => Value_Type);

   type Array_Type is record
      Default : Value_Type := Default_Value;
      Cells   : Maps.Map;
   end record;

   type Cell_Type is record
      Column : Column_Type;
      Value  : Value_Type;
   end record;

   type Cell_Array_Type is array (Positive range <>) of Cell_Type;

   --  Set the value at the given [row, column] position.
   --  A value equal to Default is not stored.
   procedure Set (Into   : in out Array_Type;
                  Row    : in Row_Type;
                  Column : in Column_Type;
                  Value  : in Value_Type);

   --  Get the value at the given [row, column] position.
   --  Returns Default if there is no cell at [row, column].
   function Get (From   : in Array_Type;
                 Row    : in Row_Type;
                 Column : in Column_Type) return Value_Type;

   --  Update the value at the given [row, column] position
   --  calling the Process function to get the new value.  If the cell
   --  does not exist, the Process function is called with the default
   --  value and if the function returns another value, a cell is created
   --  for the [row, column].
   procedure Update (Into    : in out Array_Type;
                     Row     : in Row_Type;
                     Column  : in Column_Type;
                     Process : not null access
                       function (Value : in Value_Type) return Value_Type);

   --  Fill the row with the cells given in the array.
   procedure Fill (Into   : in out Array_Type;
                   Row    : in Row_Type;
                   Cells  : in Cell_Array_Type);

   --  Execute an operation on every cell which is defined and update that
   --  cell's value by calling the Operator function.
   procedure Apply (Into     : in out Array_Type;
                    Operator : not null access
                      function (Value : in Value_Type) return Value_Type);

   --  Merge the sparse array `From` in `Into` and call the `Process` procedure
   --  when a cell is already defined in `From`.
   procedure Merge (Into : in out Array_Type;
                    From : in Array_Type;
                    Process : not null access
                       function (First, Second : in Value_Type) return Value_Type);

   --  Return the number of rows in the array.
   function Row_Count (From : in Array_Type) return Natural;

   --  Get the last row index.
   function Max_Row (From : in Array_Type) return Row_Type with
     Pre => not From.Cells.Is_Empty;

   --  Get the last column index.
   function Max_Column (From : in Array_Type) return Column_Type with
     Pre => not From.Cells.Is_Empty;

   --  Compute the sum of products of cells between the two rows.
   --  If a cell is missing in one row, use the default value.
   --  If a cell is missing in both rows, nothing is added to the sum.
   generic
      type Float_Type is digits <>;
      with function "*" (Left, Right : Value_Type) return Float_Type is <>;
   function Product (First      : in Array_Type;
                     First_Row  : in Row_Type;
                     Second     : in Array_Type;
                     Second_Row : in Row_Type) return Float_Type;

   --  Merge values of cells of the given row by using the `Merger` function
   --  and applying an optional type conversion to `Result_Type`. The `Initial`
   --  value is used as first value passed to the `Merger` function.
   generic
      type Result_Type is private;
      with function Merger (Global : Result_Type;
                            Value  : Value_Type) return Result_Type is <>;
   function Merge_Row (From    : in Array_Type;
                       Row     : in Row_Type;
                       Initial : in Result_Type) return Result_Type;

   type Count_Column_Array is array (Column_Type range <>) of Natural;

   --  Return an array indexed by the column indicating the number of cells
   --  for that column.
   function Count_Cells (From : in Array_Type) return Count_Column_Array with
     Pre => not From.Cells.Is_Empty;

end SCI.Sparse.COO_Arrays;
