-- --------------------------------------------------------------------
--  sci-vectorizers-indefinite_counters -- count occurrence of tokens for the vector
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Containers.Indefinite_Ordered_Maps;
with SCI.Sparse.COO_Arrays;

--  == Indefinite counters ==
--  The `SCI.Vectorizers.Indefinite_Counters` is similar to the
--  `SCI.Vectorizers.Counters` package but allows to use indefinite types for
--  the `Token_Type`.  For example, it can be used to use a `String` for the
--  `Token_Type` as follows:
--
--    package Token_Counters is
--       new SCI.Vectorizers.Indefinite_Counters (Token_Type => String,
--                                                Arrays => Counter_Arrays,
--                                                "<" => "<");
generic
   type Token_Type (<>) is private;
   with package Arrays is new SCI.Sparse.COO_Arrays (<>);
   with function "<" (Left, Right : Token_Type) return Boolean is <>;
package SCI.Vectorizers.Indefinite_Counters is

   subtype Value_Type is Arrays.Value_Type;
   subtype Row_Type is Arrays.Row_Type;
   subtype Column_Type is Arrays.Column_Type;

   package Token_Maps is
     new Ada.Containers.Indefinite_Ordered_Maps (Key_Type     => Token_Type,
                                                 Element_Type => Column_Type,
                                                 "<"          => "<",
                                                 "="          => Arrays."=");

   type Vectorizer_Type is record
      Tokens      : Token_Maps.Map;
      Counters    : Arrays.Array_Type;
      Last_Column : Column_Type := Column_Type'First;
   end record;

   --  Record a new token for the given row.  The token is searched in
   --  the tokens map which gives the associated column in the sparse array.
   --  If the token is not found, the token is inserted and integrated as
   --  a new column in the sparse array.  The cell at [row, column] is
   --  then incremented.
   procedure Add_Token (Into  : in out Vectorizer_Type;
                        Row   : in Row_Type;
                        Token : in Token_Type;
                        Increment : not null access
                          function (Value : in Value_Type) return Value_Type);

   procedure Add_Token (Into  : in out Vectorizer_Type;
                        Row   : in Row_Type;
                        Token : in Token_Type;
                        Increment : not null access
                          function (Value : in Value_Type) return Value_Type;
                        Value : out Column_Type);

end SCI.Vectorizers.Indefinite_Counters;
