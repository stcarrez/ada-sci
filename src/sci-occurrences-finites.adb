-- --------------------------------------------------------------------
--  sci-occurrences -- helper to identify occurrences of a given item
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Occurrences.Finites is

   --  ------------------------------
   --  Add the item in the set.  If the item already exists, add the value
   --  to the current count.
   --  ------------------------------
   procedure Add (Set   : in out Sets.Set;
                  Item  : in Element_Type;
                  Value : in Occurrence_Type) is
      C   : Occurrence := (Element    => Item,
                           Occurrence => Value);
      Pos : constant Sets.Cursor := Set.Find (C);
   begin
      if Sets.Has_Element (Pos) then
         C.Occurrence := C.Occurrence + Set.Constant_Reference (Pos).Occurrence;
         Set.Replace_Element (Pos, C);
      else
         Set.Insert (C);
      end if;
   end Add;

   --  ------------------------------
   --  Get the list of occurrence items sorted on the occurence value.
   --  ------------------------------
   procedure List (Set  : in Sets.Set;
                   Into : in out Vectors.Vector) is
      function "<" (Left, Right : Occurrence) return Boolean;

      function "<" (Left, Right : Occurrence) return Boolean is
      begin
         if Left.Occurrence < Right.Occurrence then
            return False;
         elsif Right.Occurrence < Left.Occurrence then
            return True;
         else
            return Left.Element < Right.Element;
         end if;
      end "<";

      package Sort is
         new Vectors.Generic_Sorting ("<" => "<");
   begin
      for Item of Set loop
         Into.Append (Item);
      end loop;
      Sort.Sort (Into);
   end List;

   --  ------------------------------
   --  Return the longest item in the list.
   --  ------------------------------
   function Longest (From : in Vectors.Vector) return Natural is
      Result : Natural := 0;
   begin
      for Item of From loop
         declare
            Len : constant Natural := Length (Item.Element);
         begin
            if Result < Len then
               Result := Len;
            end if;
         end;
      end loop;
      return Result;
   end Longest;

   --  ------------------------------
   --  Compute the sum of every element count.
   --  ------------------------------
   function Sum (From    : in Vectors.Vector;
                 Initial : in Occurrence_Type) return Occurrence_Type is
      Result : Occurrence_Type := Initial;
   begin
      for Item of From loop
         Result := Result + Item.Occurrence;
      end loop;
      return Result;
   end Sum;

   --  ------------------------------
   --  Get the occurrence value associated with the given item.
   --  Return the default value when the item is not found.
   --  ------------------------------
   function Get (Set     : in Sets.Set;
                 Item    : in Element_Type;
                 Default : in Occurrence_Type) return Occurrence_Type is
      C : constant Occurrence := (Element => Item, Occurrence => Default);
      P : constant Sets.Cursor := Set.Find (C);
   begin
      if Sets.Has_Element (P) then
         return Set.Constant_Reference (P).Occurrence;
      else
         return Default;
      end if;
   end Get;

end SCI.Occurrences.Finites;
