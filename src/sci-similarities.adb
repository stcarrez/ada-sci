-- --------------------------------------------------------------------
--  sci-similarities -- compute similarities between sets
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
package body SCI.Similarities is

   function Jaccard (Count1, Count2, Union_Count : in Natural)
                     return Conversions.Element is
   begin
      if Union_Count = 0 then
         return Conversions.From_Float (0.0);
      end if;
      return Conversions.From_Float (Float (Count1 + Count2 - Union_Count) / Float (Union_Count));
   end Jaccard;

   function Sorensen_Dice (Count1, Count2, Inter_Count : in Natural)
                           return Conversions.Element is
      Total : constant Natural := Count1 + Count2;
   begin
      if Total = 0 then
         return Conversions.From_Float (0.0);
      end if;
      return Conversions.From_Float (Float (2 * Inter_Count) / Float (Total));
   end Sorensen_Dice;

   function Tversky (Inter_Count, C1, C2 : in Natural;
                     Alpha, Beta : in Tversky_Weight) return Conversions.Element is
      A : constant Float := Alpha * Float (C1);
      B : constant Float := Beta * Float (C2);
   begin
      if Inter_Count = 0 and then A = 0.0 and then B = 0.0 then
         return Conversions.From_Float (0.0);
      end if;
      return Conversions.From_Float
        (Float (Inter_Count) / (Float (Inter_Count) + A + B));
   end Tversky;

end SCI.Similarities;
