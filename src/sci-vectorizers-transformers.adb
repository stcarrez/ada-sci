-- --------------------------------------------------------------------
--  sci-vectorizers-transformers -- apply TF or TIDF transformations
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Numerics.Generic_Elementary_Functions;

package body SCI.Vectorizers.Transformers is

   package Functions is
      new Ada.Numerics.Generic_Elementary_Functions (Frequency_Type);

   function Sum_Value (Sum : Frequency_Type;
                       Value : Arrays.Value_Type)
                       return Frequency_Type is (Sum + Convert (Value));

   function Sum is
     new Arrays.Merge_Row (Result_Type => Frequency_Type,
                           Merger      => Sum_Value);

   function IDF (From : in Arrays.Array_Type;
                 Mode : in IDF_Mode := IDF_Smooth) return Frequency_Array is
   begin
      return IDF (From, Arrays.Row_Count (From), Mode);
   end IDF;

   function IDF (From      : in Arrays.Array_Type;
                 Doc_Count : in Natural;
                 Mode      : in IDF_Mode := IDF_Smooth) return Frequency_Array is
      Row_Count : constant Frequency_Type := Frequency_Type (Doc_Count);
      Count     : constant Arrays.Count_Column_Array := Arrays.Count_Cells (From);
      Result    : Frequency_Array (Count'Range) := (others => 0.0);
   begin
      case Mode is
         when IDF_Normal =>
            for I in Count'Range loop
               Result (I) := Functions.Log (Row_Count / Frequency_Type (Count (I)));
            end loop;

         when IDF_Smooth =>
            for I in Count'Range loop
               Result (I) := 1.0 + Functions.Log (Row_Count / (1.0 + Frequency_Type (Count (I))));
            end loop;

         when IDF_Probabilistic =>
            for I in Count'Range loop
               Result (I) := Functions.Log
                 ((Row_Count - Frequency_Type (Count (I)) / Frequency_Type (Count (I))));
            end loop;
      end case;
      return Result;
   end IDF;

   procedure TIDF (From     : in Arrays.Array_Type;
                   Doc_Freq : in Frequency_Array;
                   Into     : in out Frequency_Arrays.Array_Type) is
      use type Arrays.Row_Type;
      Pos      : Arrays.Maps.Cursor := From.Cells.First;
   begin
      while Arrays.Maps.Has_Element (Pos) loop
         declare
            Row : constant Row_Type := Arrays.Maps.Key (Pos).Row;
            N   : constant Frequency_Type := Sum (From, Row, 0.0);
         begin
            loop
               declare
                  Index : constant Arrays.Index_Type := Arrays.Maps.Key (Pos);
                  Freq  : Frequency_Type;
               begin
                  exit when Index.Row /= Row;
                  if Index.Column in Doc_Freq'Range then
                     Freq := Convert (Arrays.Maps.Element (Pos)) / N;
                     Freq := Doc_Freq (Index.Column) * Freq;
                     Into.Cells.Insert ((Row, Index.Column), Freq);
                  end if;
                  Arrays.Maps.Next (Pos);
                  if not Arrays.Maps.Has_Element (Pos) then
                     return;
                  end if;
               end;
            end loop;
         end;
      end loop;
   end TIDF;

   procedure TIDF (From : in Arrays.Array_Type;
                   Into : in out Frequency_Arrays.Array_Type) is
      Doc_Freq : constant Frequency_Array := IDF (From);
   begin
      TIDF (From, Doc_Freq, Into);
   end TIDF;

end SCI.Vectorizers.Transformers;
