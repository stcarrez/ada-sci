-- --------------------------------------------------------------------
--  sci-similarities-indefinite_ordered_sets -- similarities with Indefinite_Ordered_Sets
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Similarities.Indefinite_Ordered_Sets is

   use type Sets.Set;

   --  ------------------------------
   --  Compute the Jaccard similarity between the two sets.
   --  1.0 * (Set1.Length + Set2.Length - Union.Length) / Union.Length
   --  ------------------------------
   function Jaccard (Set1, Set2 : in Sets.Set) return Value_Type is
      L1 : constant Natural := Natural (Set1.Length);
      L2 : constant Natural := Natural (Set2.Length);
   begin
      if L1 = L2 and then (L1 = 0 or else Set1 = Set2) then
         return Conversions.From_Float (1.0);
      end if;
      declare
         U : Sets.Set;
         C : Natural;
      begin
         U := Set1;
         U.Union (Set2);
         C := Natural (U.Length);
         return Jaccard (L1, L2, C);
      end;
   end Jaccard;

   --  ------------------------------
   --  Compute the Sorensen Dice similarity between the two sets:
   --  2.0 * Inter.Length / (Set1.Length + Set2.Length)
   --  ------------------------------
   function Sorensen_Dice (Set1, Set2 : in Sets.Set) return Value_Type is
      L1 : constant Natural := Natural (Set1.Length);
      L2 : constant Natural := Natural (Set2.Length);
   begin
      if L1 = L2 and then (L1 = 0 or else Set1 = Set2) then
         return Conversions.From_Float (1.0);
      end if;
      declare
         I : Sets.Set;
         C : Natural;
      begin
         I := Set1;
         I.Intersection (Set2);
         C := Natural (I.Length);
         return Sorensen_Dice (L1, L2, C);
      end;
   end Sorensen_Dice;

   --  ------------------------------
   --  Compute the Tversky index between the two sets:
   --  Inter / (Inter + Alpha * (Set1 / Set2) + Beta * (Set2 / Set1))
   --  ------------------------------
   function Tversky (Set1, Set2  : in Sets.Set;
                     Alpha, Beta : in Tversky_Weight) return Value_Type is
      L1 : constant Natural := Natural (Set1.Length);
      L2 : constant Natural := Natural (Set2.Length);
   begin
      if L1 = L2 and then (L1 = 0 or else Set1 = Set2) then
         return Conversions.From_Float (1.0);
      end if;
      declare
         I  : Natural := 0;
         C1 : Natural := 0;
         C2 : Natural := 0;
      begin
         for E of Set1 loop
            if Set2.Contains (E) then
               I := I + 1;
            else
               C1 := C1 + 1;
            end if;
         end loop;
         for E of Set2 loop
            if not Set1.Contains (E) then
               C2 := C2 + 1;
            end if;
         end loop;
         return Tversky (I, C1, C2, Alpha, Beta);
      end;
   end Tversky;

end SCI.Similarities.Indefinite_Ordered_Sets;
