-- --------------------------------------------------------------------
--  sci-similarities-coo_arrays -- similarities with sparse arrays
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with SCI.Numbers;
with SCI.Sparse.COO_Arrays;
generic
   with package Arrays is new SCI.Sparse.COO_Arrays (<>);
   with package Conversions is new SCI.Numbers.Conversion (<>);
   with function To_Float (Value : in Arrays.Value_Type) return Float;
package SCI.Similarities.COO_Arrays is

   subtype Row_Type is Arrays.Row_Type;
   subtype Value_Type is Conversions.Element;

   function "*" (Left, Right : Arrays.Value_Type) return Float;

   function Sum_Square (Left  : Float;
                        Right : Arrays.Value_Type) return Float;

   --  Compute Sum of produce of cells for a given row between two tables:
   --  \sum_{i=1}^{N} {A[A_Row][i] * B[B_Row][i]}
   function Multiply is
     new Arrays.Product (Float_Type => Float);

   --  Compute Sum of cell squares for a given row:
   --  \sum_{i=1}^{N} {A[row][i]^2}
   function Square is
     new Arrays.Merge_Row (Result_Type => Float,
                           Merger => Sum_Square);

   --  Compute Sqrt of sum of cell squares for a given row:
   --  \sqrt{\sum_{i=1}^{N} {A[row][i]^2}}
   function Sqrt_Square (From : in Arrays.Array_Type;
                         Row  : in Row_Type) return Float;

   --  Compute the cosine similarity from the sparse array cells between
   --  the two rows:
   --  \frac{\sum_{i=1}^{N} {A[A_Row][i] * B[B_Row][i]}}
   --     {\sqrt{\sum_{i=1}^{N} {A[A_row][i]^2}} * \sqrt{\sum_{i=1}^{N} {B[B_row][i]^2}}}
   function Cosine (A     : in Arrays.Array_Type;
                    A_Row : in Row_Type;
                    B     : in Arrays.Array_Type;
                    B_Row : in Row_Type) return Value_Type;
   function Cosine (A      : in Arrays.Array_Type;
                    A_Row  : in Row_Type;
                    B      : in Arrays.Array_Type;
                    B_Row  : in Row_Type;
                    Sqrt_B : in Float) return Value_Type;
   function Cosine (A      : in Arrays.Array_Type;
                    A_Row  : in Row_Type;
                    B      : in Arrays.Array_Type;
                    B_Row  : in Row_Type;
                    Sqrt_A : in Float;
                    Sqrt_B : in Float) return Value_Type;

end SCI.Similarities.COO_Arrays;
