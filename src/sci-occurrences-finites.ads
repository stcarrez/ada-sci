-- --------------------------------------------------------------------
--  sci-occurrences-finites -- helper to identify occurrences of a given item
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  == Finite Ordered Sets ==
--  The `SCI.Occurrences.Finites` package provides a ordered set where
--  each element added is associated with an `Occurrence_Type`.  A simple
--  occurrence of words can be created with:
--
--     package Word_Occurrences is
--        new SCI.Occurrences.Finites
--           (Element_Type => Ada.Strings.Unbounded.Unbounded_String,
--            Occurrence_Type => Natural);
--
--  When items are added to the set, the `Occurrence_Type` values are
--  merged with existing values.  This is done by the generic `Add`
--  procedure which must be instantiated with a `+` function that
--  implements such merge.
--
--     procedure Add is new Word_Occurrences.Add;
--
--  Operations of the package are used in several steps.  In a first
--  step, the items are collected and added to the set by using the
--  `Add` procedure.  Once all items are added, you will use:
--
--  * the `Get` function can be used to retrieve the occurence value
--  associated with an item.
--  * the `List` procedure to extract a list of items and sort them
--  according to a comparison function that you provide for the
--  instantiation of the `List` procedure.
--  * the `Sum` and `Longest` procedures are helper functions that
--  operate on the result produced by the `List` procedure.
with Ada.Containers.Ordered_Sets;
with Ada.Containers.Vectors;
generic
   type Element_Type is private;
   type Occurrence_Type is private;
   with function "<" (Left, Right : Element_Type) return Boolean is <>;
package SCI.Occurrences.Finites is

   type Occurrence is record
      Element    : Element_Type;
      Occurrence : Occurrence_Type;
   end record;

   --  Compare the two occurrence only on their name.
   function "<" (Left, Right : Occurrence)
                 return Boolean is (Left.Element < Right.Element);
   function Same (Left, Right : Occurrence)
                  return Boolean is (Left.Element = Right.Element);

   package Vectors is
     new Ada.Containers.Vectors (Positive, Occurrence);
   subtype Vector is Vectors.Vector;

   package Sets is
     new Ada.Containers.Ordered_Sets (Occurrence, "<", Same);
   subtype Set is Sets.Set;

   --  Add the item in the set.  If the item already exists, add the value
   --  to the current count.
   generic
      with function "+" (Left, Right : Occurrence_Type) return Occurrence_Type is <>;
   procedure Add (Set   : in out Sets.Set;
                  Item  : in Element_Type;
                  Value : in Occurrence_Type);

   --  Get the list of occurrence items sorted on the occurence value.
   generic
      with function "<" (Left, Right : Occurrence_Type) return Boolean is <>;
   procedure List (Set  : in Sets.Set;
                   Into : in out Vectors.Vector);

   --  Return the longest item in the list.
   generic
      with function Length (Item : Element_Type) return Natural is <>;
   function Longest (From : in Vectors.Vector) return Natural;

   --  Compute the sum of every element count.
   generic
      with function "+" (Left, Right : Occurrence_Type) return Occurrence_Type is <>;
   function Sum (From    : in Vectors.Vector;
                 Initial : in Occurrence_Type) return Occurrence_Type;

   --  Get the occurrence value associated with the given item.
   --  Return the default value when the item is not found.
   function Get (Set     : in Sets.Set;
                 Item    : in Element_Type;
                 Default : in Occurrence_Type) return Occurrence_Type;

end SCI.Occurrences.Finites;
