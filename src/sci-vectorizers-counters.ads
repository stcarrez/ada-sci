-- --------------------------------------------------------------------
--  sci-vectorizers-counters -- count occurrence of tokens for the vector
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------
with Ada.Containers.Ordered_Maps;
with SCI.Sparse.COO_Arrays;

--  == Counters ==
--  The `SCI.Vectorizers.Counters` is a vectorizer that counts the occurrence
--  of tokens and builds a vector of these counters.  The `Vectorizer_Type`
--  contains an ordered map of tokens represented by `Token_Type` and for
--  each of them it indicates a column index within the final vector.
--  When tokens are recorded for a document, it looks for a token in the map,
--  finds the associated column and increments a counter for the document and
--  token.  If the token is not found, it is inserted in the map and associated
--  with a new column in the vector.  For example, the package can be
--  instantiated as follows:
--
--    package Token_Counters is
--       new SCI.Vectorizers.Counters (Token_Type => Unbounded_String,
--                                     Arrays => Counter_Arrays,
--                                     "<" => "<");
--
--  An instance of the vectorizer is declared and configured:
--
--     V : Token_Counters.Vectorizer_Type;
--     ...
--        V.Counters.Default := 0;
--
--  The vectorizer is filled with documents and tokens by using the
--  `Add_Token` procedure with the `Row` representing the document.
--  Because the vector cell can be any Ada private type, it is necessary
--  to provide an `Increment` function that gets the current value and
--  increment it.
--
--     function Increment (Value : in Natural) return Natural is (Value + 1);
--     Token_Counters.Add_Token (Into      => V,
--                               Token     => Item,
--                               Increment => Increment'Access);
--
--  After filling the vectorizer instance, it will contain in `V.Counters`
--  the cells which count the token occurrence per document scanned.
--  The result can be used to compute similarities between different
--  documents (known as rows).
generic
   type Token_Type is private;
   with package Arrays is new SCI.Sparse.COO_Arrays (<>);
   with function "<" (Left, Right : Token_Type) return Boolean is <>;
package SCI.Vectorizers.Counters is

   subtype Value_Type is Arrays.Value_Type;
   subtype Row_Type is Arrays.Row_Type;
   subtype Column_Type is Arrays.Column_Type;

   package Token_Maps is
     new Ada.Containers.Ordered_Maps (Key_Type     => Token_Type,
                                      Element_Type => Column_Type,
                                      "<"          => "<",
                                      "="          => Arrays."=");

   type Vectorizer_Type is record
      Tokens      : Token_Maps.Map;
      Counters    : Arrays.Array_Type;
      Last_Column : Column_Type := Column_Type'First;
   end record;

   --  Record a new token for the given row.  The token is searched in
   --  the tokens map which gives the associated column in the sparse array.
   --  If the token is not found, the token is inserted and integrated as
   --  a new column in the sparse array.  The cell at [row, column] is
   --  then incremented.
   procedure Add_Token (Into  : in out Vectorizer_Type;
                        Row   : in Row_Type;
                        Token : in Token_Type;
                        Increment : not null access
                          function (Value : in Value_Type) return Value_Type);

end SCI.Vectorizers.Counters;
