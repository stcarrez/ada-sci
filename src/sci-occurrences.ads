-- --------------------------------------------------------------------
--  sci-occurrences -- helper to identify occurrences of a given item
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  = Occurrences =
--  The `SCI.Occurrences` children packages provide generic packages to
--  compute occurrence of items.
--
--  @include sci-occurrences-arrays.ads
--  @include sci-occurrences-finites.ads
package SCI.Occurrences with Pure is

end SCI.Occurrences;
