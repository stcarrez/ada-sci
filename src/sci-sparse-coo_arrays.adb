-- --------------------------------------------------------------------
--  sci-sparse-coo_arrays -- sparse arrays using COO implementation
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

package body SCI.Sparse.COO_Arrays is

   --  ------------------------------
   --  Set the value at the given [row, column] position.
   --  A value equal to Default is not stored.
   --  ------------------------------
   procedure Set (Into   : in out Array_Type;
                  Row    : in Row_Type;
                  Column : in Column_Type;
                  Value  : in Value_Type) is
      Key : constant Index_Type := (Row, Column);
      Pos : Maps.Cursor := Into.Cells.Find (Key);
   begin
      if Value = Into.Default then
         if Maps.Has_Element (Pos) then
            Into.Cells.Delete (Pos);
         end if;
      elsif Maps.Has_Element (Pos) then
         Into.Cells.Reference (Pos).Element.all := Value;
      else
         Into.Cells.Insert (Key, Value);
      end if;
   end Set;

   --  ------------------------------
   --  Get the value at the given [row, column] position.
   --  Returns Default if there is no cell at [row, column].
   --  ------------------------------
   function Get (From   : in Array_Type;
                 Row    : in Row_Type;
                 Column : in Column_Type) return Value_Type is
      Key : constant Index_Type := (Row, Column);
      Pos : constant Maps.Cursor := From.Cells.Find (Key);
   begin
      if Maps.Has_Element (Pos) then
         return Maps.Element (Pos);
      else
         return From.Default;
      end if;
   end Get;

   --  ------------------------------
   --  Update the value at the given [row, column] position
   --  calling the Process function to get the new value.  If the cell
   --  does not exist, the Process function is called with the default
   --  value and if the function returns another value, a cell is created
   --  for the [row, column].
   --  ------------------------------
   procedure Update (Into    : in out Array_Type;
                     Row     : in Row_Type;
                     Column  : in Column_Type;
                     Process : not null access
                       function (Value : in Value_Type) return Value_Type) is
      Key : constant Index_Type := (Row, Column);
      Pos : Maps.Cursor := Into.Cells.Find (Key);
   begin
      if Maps.Has_Element (Pos) then
         declare
            Value : constant Value_Type := Process (Maps.Element (Pos));
         begin
            if Value = Into.Default then
               Into.Cells.Delete (Pos);
            else
               Into.Cells.Reference (Pos).Element.all := Value;
            end if;
         end;
      else
         declare
            Value : constant Value_Type := Process (Into.Default);
         begin
            if Value /= Into.Default then
               Into.Cells.Insert (Key, Value);
            end if;
         end;
      end if;
   end Update;

   --  ------------------------------
   --  Fill the row with the cells given in the array.
   --  ------------------------------
   procedure Fill (Into   : in out Array_Type;
                   Row    : in Row_Type;
                   Cells  : in Cell_Array_Type) is
   begin
      for Cell of Cells loop
         Set (Into, Row, Cell.Column, Cell.Value);
      end loop;
   end Fill;

   --  ------------------------------
   --  Execute an operation on every cell which is defined and update that
   --  cell's value by calling the Operator function.
   --  ------------------------------
   procedure Apply (Into     : in out Array_Type;
                    Operator : not null access
                      function (Value : in Value_Type) return Value_Type) is
   begin
      for Pos in Into.Cells.Iterate loop
         Into.Cells.Reference (Pos).Element.all
           := Operator (Into.Cells.Reference (Pos).Element.all);
      end loop;
   end Apply;

   --  ------------------------------
   --  Merge the sparse array `From` in `Into` and call the `Process` procedure
   --  when a cell is already defined in `From`.
   --  ------------------------------
   procedure Merge (Into : in out Array_Type;
                    From : in Array_Type;
                    Process : not null access
                       function (First, Second : in Value_Type) return Value_Type) is
   begin
      for Cell in From.Cells.Iterate loop
         declare
            Key : constant Index_Type := Maps.Key (Cell);
            Val : constant Value_Type := Maps.Element (Cell);
            Pos : constant Maps.Cursor := Into.Cells.Find (Key);
         begin
            if Maps.Has_Element (Pos) then
               Into.Cells.Reference (Pos).Element.all := Process (Maps.Element (Pos), Val);
            else
               Into.Cells.Insert (Key, Val);
            end if;
         end;
      end loop;
   end Merge;

   --  ------------------------------
   --  Return the number of rows in the array.
   --  ------------------------------
   function Row_Count (From : in Array_Type) return Natural is
   begin
      if From.Cells.Is_Empty then
         return 0;
      end if;
      declare
         Current : Row_Type := From.Cells.First_Key.Row;
         Count   : Natural := 1;
      begin
         for Pos in From.Cells.Iterate loop
            declare
               Row : constant Row_Type := Maps.Key (Pos).Row;
            begin
               if Row /= Current then
                  Count := Count + 1;
                  Current := Row;
               end if;
            end;
         end loop;
         return Count;
      end;
   end Row_Count;

   --  ------------------------------
   --  Get the last row index.
   --  ------------------------------
   function Max_Row (From : in Array_Type) return Row_Type is
   begin
      --  Cells are sorted on the row and column.
      return From.Cells.Last_Key.Row;
   end Max_Row;

   --  ------------------------------
   --  Get the last column index.
   --  ------------------------------
   function Max_Column (From : in Array_Type) return Column_Type is
      C : Column_Type := From.Cells.First_Key.Column;
   begin
      --  We have to scan every cell.
      for Pos in From.Cells.Iterate loop
         declare
            Col : constant Column_Type := Maps.Key (Pos).Column;
         begin
            if Col > C then
               C := Col;
            end if;
         end;
      end loop;
      return C;
   end Max_Column;

   --  ------------------------------
   --  Compute the sum of products of cells between the two rows.
   --  If a cell is missing in one row, use the default value.
   --  If a cell is missing in both rows, nothing is added to the sum.
   --  ------------------------------
   function Product (First      : in Array_Type;
                     First_Row  : in Row_Type;
                     Second     : in Array_Type;
                     Second_Row : in Row_Type) return Float_Type is
      Row1   : Maps.Cursor := First.Cells.Ceiling ((First_Row, Column_Type'First));
      Row2   : Maps.Cursor := Second.Cells.Ceiling ((Second_Row, Column_Type'First));
      Result : Float_Type := 0.0;
      Value  : Float_Type;
   begin
      loop
         if Maps.Has_Element (Row1) then
            declare
               Key1 : constant Index_Type := Maps.Key (Row1);
            begin
               if Maps.Has_Element (Row2) then
                  declare
                     Key2 : constant Index_Type := Maps.Key (Row2);
                  begin
                     exit when Key1.Row /= First_Row and then Key2.Row /= Second_Row;
                     if Key1.Row /= First_Row then
                        Value := First.Default * Maps.Element (Row2);
                        Maps.Next (Row2);
                     elsif Key2.Row /= Second_Row then
                        Value := Maps.Element (Row1) * Second.Default;
                        Maps.Next (Row1);
                     elsif Key1.Column = Key2.Column then
                        Value := Maps.Element (Row1) * Maps.Element (Row2);
                        Maps.Next (Row1);
                        Maps.Next (Row2);
                     elsif Key1.Column < Key2.Column then
                        Value := Maps.Element (Row1) * Second.Default;
                        Maps.Next (Row1);
                     else
                        Value := First.Default * Maps.Element (Row2);
                        Maps.Next (Row2);
                     end if;
                     Result := Result + Value;
                  end;
               else
                  exit when Key1.Row /= First_Row;
                  Value := Second.Default * Maps.Element (Row1);
                  Result := Result + Value;
                  Maps.Next (Row1);
               end if;
            end;
         else
            exit when not Maps.Has_Element (Row2);
            declare
               Key2  : constant Index_Type := Maps.Key (Row2);
            begin
               exit when Key2.Row /= Second_Row;
               Value := First.Default * Maps.Element (Row2);
               Result := Result + Value;
               Maps.Next (Row2);
            end;
         end if;
      end loop;
      return Result;
   end Product;

   --  ------------------------------
   --  Merge values of cells of the given row by using the `Merger` function
   --  and applying an optional type conversion to `Result_Type`. The `Initial`
   --  value is used as first value passed to the `Merger` function.
   --  ------------------------------
   function Merge_Row (From    : in Array_Type;
                       Row     : in Row_Type;
                       Initial : in Result_Type) return Result_Type is
      Result : Result_Type := Initial;
      Pos    : Maps.Cursor := From.Cells.Ceiling ((Row, Column_Type'First));
   begin
      while Maps.Has_Element (Pos) loop
         declare
            Key  : constant Index_Type := Maps.Key (Pos);
         begin
            exit when Key.Row /= Row;
            Result := Merger (Result, Maps.Element (Pos));
            Maps.Next (Pos);
         end;
      end loop;
      return Result;
   end Merge_Row;

   function Count_Cells (From : in Array_Type) return Count_Column_Array is
      C   : constant Column_Type := From.Cells.First_Key.Column;
      Min : Column_Type := C;
      Max : Column_Type := C;
   begin
      --  We have to scan every cell.
      for Pos in From.Cells.Iterate loop
         declare
            Col : constant Column_Type := Maps.Key (Pos).Column;
         begin
            if Col > Max then
               Max := Col;
            end if;
            if Col < Min then
               Min := Col;
            end if;
         end;
      end loop;
      declare
         Result : Count_Column_Array (Min .. Max) := (others => 0);
      begin
         for Pos in From.Cells.Iterate loop
            declare
               Col : constant Column_Type := Maps.Key (Pos).Column;
            begin
               Result (Col) := Result (Col) + 1;
            end;
         end loop;
         return Result;
      end;
   end Count_Cells;

end SCI.Sparse.COO_Arrays;
