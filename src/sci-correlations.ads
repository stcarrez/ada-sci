-- --------------------------------------------------------------------
--  sci-correlations -- compute correlations between values
--  Written by Stephane Carrez (Stephane.Carrez@gmail.com)
--  SPDX-License-Identifier: Apache-2.0
-----------------------------------------------------------------------

--  = Correlations =
--  To use the `Correlations` package, it is first necessary to instantiate
--  the `SCI.Correlations` package with a `Float_Type` used for the internal
--  computation.  This floating point type must have enough digits and allow
--  enough value ranges for proper statistics calculations.
--
--     package Double_Correlations is new SCI.Correlations (Double);
--
--  @include sci-correlations-pearson.ads
with Ada.Numerics.Generic_Elementary_Functions;
generic
   type Float_Type is digits <>;
package SCI.Correlations with Pure is

   package Functions is
      new Ada.Numerics.Generic_Elementary_Functions (Float_Type);

end SCI.Correlations;
