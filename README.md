# SCI Ada

[![Alire](https://img.shields.io/endpoint?url=https://alire.ada.dev/badges/sciada.json)](https://alire.ada.dev/crates/sciada)
[![Build Status](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/ada-sci/badges/build.json)](https://porion.vacs.fr/porion/projects/view/ada-sci/summary)
[![Test Status](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/ada-sci/badges/tests.json)](https://porion.vacs.fr/porion/projects/view/ada-sci/xunits)
[![Coverage](https://img.shields.io/endpoint?url=https://porion.vacs.fr/porion/api/v1/projects/ada-sci/badges/coverage.json)](https://porion.vacs.fr/porion/projects/view/ada-sci/summary)
[![Documentation Status](https://readthedocs.org/projects/ada-sci/badge/?version=latest)](https://ada-sci.readthedocs.io/en/latest/?badge=latest)

SCI Ada provides a collections of algorithms commonly used for data analysis:

- [SCI.Correlations.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-correlations.ads?ref_type=heads) computes correlations between values,
- [SCI.Occurrences.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-occurrences.ads?ref_type=heads) helps in identifying occurrences of items,
- [SCI.Similarities.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-similarities.ads?ref_type=heads) provides metrics to compute similarities between sets.
- [SCI.Sparse.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-sparse.ads?ref_type=heads) defines sparse arrays.
- [SCI.Statistics.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-statistics.ads?ref_type=heads) defines some classical statistics operations.
- [SCI.Vectorizers.*](https://gitlab.com/stcarrez/ada-sci/-/blob/main/src/sci-vectorizers.ads?ref_type=heads) transforms a list of tokens to a vector.

## Alire setup

```
alr with sciada
```

## Documentation

- [Programmer's Guide](https://ada-sci.readthedocs.io/en/latest/)

## Algorithms

### Similarity Algorithms

The `SCI.Similarities.*` packages provide various metrics to compute similarities between two sets.

* [Jaccard](https://en.wikipedia.org/wiki/Jaccard_index)
* [Sørensen–Dice](https://en.wikipedia.org/wiki/S%C3%B8rensen%E2%80%93Dice_coefficient)
* [Tversky](https://en.wikipedia.org/wiki/Tversky_index)
* [Cosine similarity](https://en.wikipedia.org/wiki/Cosine_similarity)

## Example

- [similar.adb](https://gitlab.com/stcarrez/ada-sci/-/blob/main/examples/src/similar.adb?ref_type=heads)


